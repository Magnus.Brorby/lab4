package datastructure;

import cellular.CellState;
import java.util.Random;

public class CellGrid implements IGrid {

    Random rand = new Random();
    private int rows;
    private int columns;
    private CellState initialState;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];

        for (int i = 0; i < rows; i++) {

            for (int j = 0; j < columns; j++) {
                grid[i][j] = initialState;
                
            }
            
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if ((row >= 0 & row < numRows()) && (column >= 0 & column < numColumns())) {
            grid[row][column] = element;
        }
        else {
            throw new IndexOutOfBoundsException();
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        if ((row >= 0 & row < numRows()) && (column >= 0 & column < numColumns())) {
            return grid[row][column];
        }
        else {
            throw new IndexOutOfBoundsException();
        }
        //return null;
    }

    @Override
    public IGrid copy() {
        IGrid gcopy = new CellGrid(rows, columns, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                gcopy.set(i, j, get(i, j));
            }
        }
        return gcopy;
    }
    
}
